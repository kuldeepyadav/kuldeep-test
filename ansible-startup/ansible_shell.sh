#!/bin/bash
inventory_file="$1"
prefix="ansible all -v -i $inventory_file -m shell -a "

echo "Prefix: $prefix"
echo

$prefix 'date' --ask-pass
$prefix 'ls -l' --ask-pass
$prefix 'cal' --ask-pass
$prefix 'echo "YipZZZZ" | tr Z e' --ask-pass
$prefix 'echo "Yipeee!!"' --ask-pass
